<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Umroh</title>
        {{-- favicon --}}
        <link rel="icon" type="image/png" sizes="96x96" href="{{asset('frontend/images/favicon.png')}}">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        {{-- frontend --}}
        <link href="{{ url('frontend/libraries/bootstrap-4.5.3/css/bootstrap.css')}}" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Assistant:wght@200;300;400;500;600;700;800&family=Playfair+Display:ital,wght@0,500;0,600;0,700;0,800;0,900;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
        <link href="{{ url('frontend/styles/main.css')}}" rel="stylesheet">

        <link rel="stylesheet" href="{{ url('backend/fancybox/jquery.fancybox.min.css')}}">
        {{-- custom css for admin --}}
        <link rel="stylesheet" href="{{ asset('backend/mycss/style.css') }}">
        <!-- Font Awesome 4.7.0 -->
        <link rel="stylesheet" href="{{ asset('backend/font-awesome/css/font-awesome.min.css') }}">
    </head>
    <body class="{{ Request::is('admin/*') ? 'bg-light' : '' }}">
        <div id="umroh">
            <app></app>
        </div>

        {{-- frontend --}}
        <script src="{{ url('frontend/libraries/jquery/jquery-3.5.1.min.js')}}"></script>
         <!-- Bootstrap. pakai yg bundle, supaya popper.js include disini -->
        <script src="{{ url('frontend/libraries/bootstrap-4.5.3/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{ url('backend/fancybox/jquery.fancybox.min.js')}}"></script>
        <script src="{{ url('frontend/libraries/retina/retina.min.js')}}"></script>

        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
<!-- tag <app></app>, dimana tag tersebut merupakan custom tag yang dibuat dari component Vue.js dan pastikan tag div yang mengapitnya memiliki id appku. Adapun tag lainnya hanya me-load file js dan css. -->
