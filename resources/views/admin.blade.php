<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Umroh</title>
        {{-- favicon --}}
        <link rel="icon" type="image/png" sizes="96x96" href="{{asset('frontend/images/faviconn.png')}}">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        {{-- admin --}}
        <link href="{{ url('backend/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
        <link
            href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <link href="{{ url('backend/css/sb-admin-2.min.css')}}" rel="stylesheet">
    </head>
    <body id="page-top">
        <div id="umroh">
            <app></app>
        </div>
        
        {{-- admin --}}
        <!-- Bootstrap core JavaScript-->
        <script src="{{ url('backend/vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ url('backend/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
        <!-- Core plugin JavaScript-->
        <script src="{{ url('backend/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
        <!-- Custom scripts for all pages-->
        <script src="{{ url('backend/js/sb-admin-2.min.js')}}"></script>
        <!-- Page level plugins -->
        <script src="{{ url('backend/vendor/chart.js/Chart.min.js')}}"></script>
        <!-- Page level custom scripts -->
        <script src="{{ url('backend/js/demo/chart-area-demo.js')}}"></script>
        <script src="{{ url('backend/js/demo/chart-pie-demo.js')}}"></script>

        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
<!-- tag <app></app>, dimana tag tersebut merupakan custom tag yang dibuat dari component Vue.js dan pastikan tag div yang mengapitnya memiliki id appku. Adapun tag lainnya hanya me-load file js dan css. -->
