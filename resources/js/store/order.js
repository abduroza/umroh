import $axios from '../api.js'
import { reject } from 'lodash'

const state = () => ({
    orders: [], //menampung list data order dari database. controller index. bentuknya array sehingga banyaknya data dapat dihitung dengan length

    //menampung value dari inputan. datanya dari edit untuk store dan update. controllr store n update
    order: {
        umroh_package: '',
        order_details: [
            { user: { email : '' }, is_visa: '', doe_passport: '' }
        ],

    },
    page: 1, //mencatat paginate yg diakses, dg default page 1
    isLoading: false
})

const mutations = {
    //memasukkan ke state orders untuk ditampilkan (index)
    ASSIGN_DATA(state, payload){
        state.orders = payload
    },

    SET_PAGE(state, payload){
        state.page = payload
    },
}

const actions = {
    //request list order
    getOrders({ commit, state }, payload){
        //cek apakah di payload ada value search yg dikirmkam
        let search = typeof payload.search != 'undefined' ? payload.search : ''
        let status = typeof payload.status != 'undefined' ? payload.status : ''

        return new Promise((resolve, reject) => {
            //fetch data dari api dengan mengirimkan page dan search
            $axios.get(`/order?page=${state.page}&q=${search}&status=${status}`)
            .then((res) => {
                //simpan data ke state melalui mutations
                commit('ASSIGN_DATA', res.data)
                resolve(res.data)
            })
        })
    },
    
}
export default {
    namespaced: true,
    state,
    mutations,
    actions
}
