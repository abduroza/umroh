import $axios from '../api.js'
import { reject } from 'lodash'

const state = () => ({
    isLoading: false
})

const mutations = {
    SET_LOADING(state, payload){
        state.isLoading = payload
    }
}

const actions = {
    submit({ commit }, payload ) { //submit() digunakan pada component Login.vue
        localStorage.setItem('token', null) //menambahkan variable token dan di set null pada localstorage
        commit('SET_TOKEN', null, { root: true }) //reset token menjadi null. karena mutation ada di module root maka tambahkan { root: true }

        //menggunakan promise agar fungsi lain berjalan setelah fungsi ini selesai
        return new Promise((resolve, reject) => {
            commit('SET_LOADING', true)
            //mengirim request ke server dengan URI /login 
            //payload adalah data yg dikirimkn dari component login.vue
            $axios.post('/login', payload)
            .then((res) => {
                if(res.data.status == 'success'){
                    //local storage dan state di store.js akan diset menggunakan api dari server response
                    localStorage.setItem('token', res.data.data.api_token)
                    localStorage.setItem('user', JSON.stringify(res.data.data))
                    commit('SET_TOKEN', res.data.data.api_token, { root: true })
                    commit('SET_USER_DATA', JSON.stringify(res.data.data), { root: true })
                } else {
                    //membuat key invalid yg valuenya Email / Password salah
                    //error ini muncul selain karena err validasi di validate. misal password keliru. di validate tidak divalidasi password keliru/tak ditemukan. biar aman
                    commit('SET_ERRORS', { invalid: res.data.message }, { root: true })
                }
                commit('SET_LOADING', false)
                resolve(res.data)
            })
            .catch((err) => { //error ini berasal dari backend jika inputan tidak sesuai validate
                if(err.response.status == 422){
                    commit('SET_ERRORS', err.response.data.errors, { root: true })
                }
                commit('SET_LOADING', false)
            })
        })
    },
    register({ commit }, payload ) { //submit() digunakan pada component Login.vue
        localStorage.setItem('token', null) //menambahkan variable token dan di set null pada localstorage
        commit('SET_TOKEN', null, { root: true }) //reset token menjadi null. karena mutation ada di module root maka tambahkan { root: true }

        //menggunakan promise agar fungsi lain berjalan setelah fungsi ini selesai
        return new Promise((resolve, reject) => {
            commit('SET_LOADING', true)
            //payload adalah data yg dikirimkn dari component register.vue
            $axios.post('/register', payload, {
                //karena terdapat foto maka headernya ditambahkan multipart/form-data
                headers: {
                    'Content-Type' : 'multipart/form-data'
                }
            })
            .then((res) => {
                localStorage.setItem('token', res.data.data.api_token) //menambgakn key value token di localstorage. langsung login
                localStorage.setItem('user', JSON.stringify(res.data.data))
                commit('SET_TOKEN', res.data.data.api_token, { root: true })
                commit('SET_USER_DATA', JSON.stringify(res.data.data), { root: true })
                commit('SET_LOADING', false)
                commit('SET_SUCCESS', res.data, { root: true })
                resolve(res.data)
            })
            .catch((err) => { //error ini berasal dari backend jika inputan tidak sesuai validate
                if(err.response.status == 422){
                    commit('SET_ERRORS', err.response.data.errors, {root: true})
                } else if(err.response.status == 400){
                    commit('SET_ERRORS', err.response.data, { root: true })
                }
                commit('SET_LOADING', false)
            })
        })
    }
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}
