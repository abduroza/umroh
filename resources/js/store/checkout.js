import { reject } from 'lodash'
import $axios from '../api.js'

const state = () => ({
    //menampilkan semua data
    transaction: '',

    //untuk menambahkan member
    member: {
        email: '',
        is_visa: '',
        doe_passport: ''
    },

    isLoading: false

})

const mutations = {
    //untuk keperluan index/ get all data
    ASSIGN_DATA(state, payload){
        state.transaction = payload
    },
    CLEAR_FORM(state, payload){
        state.member = {
            email: '',
            is_visa: '',
            doe_passport: ''
        }
    },
    SET_LOADING(state, payload){
        state.isLoading = payload
    },
}

const actions = {
    viewTransaction({commit, state}, payload){
        return new Promise((resolve, reject) => {
            $axios.get(`/checkout/${payload}`)
            .then((res) => {
                commit('ASSIGN_DATA', res.data.data)
                resolve(res.data)
            })
        })
    },
    addToCart({commit, state}, payload){
        return new Promise((resolve, reject) => {
            $axios.post(`/checkout/${payload}`)
            .then((res) => {
                commit('SET_SUCCESS', res.data, { root: true })
                resolve(res.data)
            })
            .catch((err) => {
                if(err.response.status == 422){
                    commit('SET_ERRORS', err.response.data.errors, { root: true })
                } else if(err.response.status == 400){
                    commit('SET_ERRORS', err.response.data, { root: true })
                }
            })
        })
    },
    addMember({commit, state}, payload){
        return new Promise((resolve, reject) => {
            $axios.post(`/checkout/add-member/${payload}`, state.member)
            .then((res) => {
                commit('CLEAR_ERRORS', 'payload', { root: true })
                commit('SET_SUCCESS', res.data, { root: true })
                commit('CLEAR_FORM')
                resolve(res.data)
            })
            .catch((err) => {
                if(err.response.status == 422){
                    commit('SET_ERRORS', err.response.data.errors, { root: true })
                } else if(err.response.status == 400){
                    commit('SET_ERRORS', err.response.data, { root: true })
                }
            })
        })
    },
    removeMember({commit, state}, payload){
        return new Promise((resolve, reject) => {
            $axios.delete(`/checkout/remove-member/${payload}`)
            .then((res) => {
                commit('SET_SUCCESS', res.data, { root: true })
                resolve(res.data)
            })
            .catch((err) => {
                if(err.response.status == 422){
                    commit('SET_ERRORS', err.response.data.errors, { root: true })
                } else if(err.response.status == 400){
                    commit('SET_ERRORS', err.response.data, { root: true })
                }
            })
        })
    },
    makePayment({commit, state}, payload){
        return new Promise((resolve, reject) => {
            commit('SET_LOADING', true)
            $axios.put(`/checkout/confirm/${payload}`)
            .then((res) => {
                commit('SET_SUCCESS', res.data, { root: true })
                commit('SET_LOADING', false)
                resolve(res.data)
            })
            .catch((err) => {
                if(err.response.status == 422){
                    commit('SET_ERRORS', err.response.data.errors, { root: true })
                } else if(err.response.status == 400){
                    commit('SET_ERRORS', err.response.data, { root: true })
                }
                commit('SET_LOADING', false)
            })
        })
    },
    cancelBooking({commit, state}, payload){
        return new Promise((resolve, reject) => {
            $axios.delete(`/checkout/${payload}`)
            .then((res) => {
                commit('SET_SUCCESS', res.data, { root: true })
                resolve(res.data)
            })
            .catch((err) => {
                if(err.response.status == 422){
                    commit('SET_ERRORS', err.response.data.errors, { root: true })
                } else if(err.response.status == 400){
                    commit('SET_ERRORS', err.response.data, { root: true })
                }
            })
        })
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}
