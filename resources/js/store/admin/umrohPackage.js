import $axios from '../../api.js'
import { reject } from 'lodash'

const state = () => ({
    umrohPackages: [], //menampung list data umroh packages dari database. controller index. bentuknya array sehingga banyaknya data dapat dihitung dengan length

    //menampung value dari inputan. datanya dari edit untuk store dan update. controllr store n update
    umrohPackage: {
        title: '',
        slug: '',
        location: '',
        about: '',
        featured_event: '',
        food: '',
        departure_date: '',
        airline: '',
        hotel: '',
        duration: '',
        price: '',
        visa: ''
    },
    page: 1, //mencatat paginate yg diakses, dg default page 1
    isLoading: false
})

const mutations = {
    //memasukkan ke state umrohPackages untuk ditampilkan (index)
    ASSIGN_DATA(state, payload){
        state.umrohPackages = payload
    },

    SET_PAGE(state, payload){
        state.page = payload
    },

    //mengubah data state umroh package yg datanya dari edit untuk store n update
    ASSIGN_FORM(state, payload){
        state.umrohPackage = {
            title: payload.title,
            slug: payload.slug,
            location: payload.location,
            about: payload.about,
            featured_event: payload.featured_event,
            food: payload.food,
            departure_date: payload.departure_date,
            airline: payload.airline,
            hotel: payload.hotel,
            duration: payload.duration,
            price: payload.price,
            visa: payload.visa
        }
    },

    //mengosongkan form
    CLEAR_FORM(state, payload){
        state.umrohPackage = {
            title: '',
            slug: '',
            location: '',
            about: '',
            featured_event: '',
            food: '',
            departure_date: '',
            airline: '',
            hotel: '',
            duration: '',
            price: '',
            visa: ''
        }
    },
    SET_LOADING(state, payload){
        state.isLoading = payload
    }
}

const actions = {
    //request list umroh package
    getUmrohPackages({ commit, state }, payload){
        //cek apakah di payload ada value search yg dikirmkam
        let search = typeof payload != 'undefined' ? payload : ''
        return new Promise((resolve, reject) => {
            //fetch data dari api dengan mengirimkan page dan search
            $axios.get(`/admin/umroh-package?page=${state.page}&q=${search}`)
            .then((res) => {
                //simpan data ke state melalui mutations
                commit('ASSIGN_DATA', res.data)
                resolve(res.data)
            })
        })
    },
    //create a new data
    submitUmrohPackage({ commit, dispatch, state }){
        return new Promise((resolve, reject) => {
            commit('SET_LOADING', true)
            $axios.post(`/admin/umroh-package`, state.umrohPackage)
            .then((res) => {
                dispatch('getUmrohPackages').then(() => {
                    commit('SET_LOADING', false)
                    resolve(res.data)
                })
                commit('SET_SUCCESS', res.data, { root: true })
            })
            .catch((err) => {
                if(err.response.status == 422){
                    commit('SET_ERRORS', err.response.data.errors, {root: true})
                } else if(err.response.status == 400){
                    commit('SET_ERRORS', err.response.data, { root: true })
                }
                commit('SET_LOADING', false)
            })
        })
    },
    //show a data umroh package
    editUmrohPackage({commit, state}, payload){
        return new Promise((resolve, reject) => {
            $axios.get(`/admin/umroh-package/${payload}/edit`)
            .then((res) => {
                //jika berhasil data yg diperoleh di assign melalui mutation
                commit('ASSIGN_FORM', res.data.data)
                resolve(res.data)
            })
        })
    },
    //update umroh package by id. payload passing the id umroh package
    updateUmrohPackage({ state, commit }, payload){
        return new Promise((resolve, reject) => {
            commit('SET_LOADING', true)
            $axios.put(`/admin/umroh-package/${payload}`, state.umrohPackage)
            .then((res) => {
                commit('CLEAR_FORM')
                commit('SET_LOADING', false)
                commit('SET_SUCCESS', res.data, { root: true })
                resolve(res.data)
            })
            .catch((err) => {
                if(err.response.status == 422){
                    commit('SET_ERRORS', err.response.data.errors, {root: true})
                } else if(err.response.status == 400){
                    commit('SET_ERRORS', err.response.data, { root: true })
                }
                commit('SET_LOADING', false)
            })
        })
    },
    //delete a umroh package by id
    removeUmrohPackage({ dispatch, commit }, payload){
        return new Promise((resolve, reject) => {
            $axios.delete(`/admin/umroh-package/${payload}`)
            .then((res) => {
                //res kayaknya tidak berisi data, sehingga tidak perlu dikirm di resolve
                //jika berhasil fecth data terbaru
                dispatch('getUmrohPackages').then(() => resolve())
                commit('SET_SUCCESS', res.data, { root: true })
            })
            .catch((err) => {
                //kirim value error ke store.js
                commit('SET_ERRORS', err.response.data.errors, { root: true })
            })
        })
    },
    //request list umroh package
    getUmrohPackageWithoutPage({ commit, state }, payload){
        //cek apakah di payload ada value search yg dikirmkam
        let search = typeof payload != 'undefined' ? payload : ''
        return new Promise((resolve, reject) => {
            //fetch data dari api dengan mengirimkan page dan search
            $axios.get(`/admin/umroh-package/all?q=${search}`)
            .then((res) => {
                //simpan data ke state melalui mutations
                commit('ASSIGN_DATA', res.data)
                resolve(res.data)
            })
        })
    },
}
export default {
    namespaced: true,
    state,
    mutations,
    actions
}
