import $axios from '../../api.js'
import { reject } from 'lodash'

const state = () => ({
    transactions: [], //menampung list data transaction dari database. controller index. bentuknya array sehingga banyaknya data dapat dihitung dengan length

    //menampung value dari inputan. datanya dari edit untuk store dan update. controllr store n update
    transaction: {
        umroh_package: '', //ini juga dipakai oleh view.vue
        transaction_details: [ //ini juga dipakai oleh view.vue
            { user: { email : '' }, is_visa: '', doe_passport: '' }
        ],

        //ini utk view.vue
        user_id: '',
        umroh_package_id: '', 
        additional_visa: '',
        total: '',
        status: '',
        user: '',
        payments: []
    },
    umrohPackages: [],
    page: 1, //mencatat paginate yg diakses, dg default page 1
    isLoading: false
})

const mutations = {
    //memasukkan ke state transactions untuk ditampilkan (index)
    ASSIGN_DATA(state, payload){
        state.transactions = payload
    },

    SET_PAGE(state, payload){
        state.page = payload
    },

    EDIT_TRANSACTION(state, payload){
        state.transaction = payload
        // {
        //     umroh_package: payload.umroh_package,
        //     transaction_details: payload.transaction_details,
        //     tr
        // }
    },

    //mengubah data state transaction yg datanya dari viewTransaction untuk View.vue
    VIEW_TRANSACTION(state, payload){
        state.transaction = payload
    },

    //mengosongkan form
    CLEAR_FORM(state, payload){
        state.transaction = {
            user_id: '',
            umroh_package_id: '',
            additional_visa: '',
            total: '',
            status: ''
        }
    },
    SET_LOADING(state, payload){
        state.isLoading = payload
    },
    ASSIGN_DATA_UMROH(state, payload){
        state.umrohPackages = payload
    },
}

const actions = {
    //request list transaction
    getTransactions({ commit, state }, payload){
        //cek apakah di payload ada value search yg dikirmkam
        let search = typeof payload.search != 'undefined' ? payload.search : ''
        let status = typeof payload.status != 'undefined' ? payload.status : ''

        return new Promise((resolve, reject) => {
            //fetch data dari api dengan mengirimkan page dan search
            $axios.get(`/admin/transaction?page=${state.page}&q=${search}&status=${status}`)
            .then((res) => {
                //simpan data ke state melalui mutations
                commit('ASSIGN_DATA', res.data)
                resolve(res.data)
            })
        })
    },
    // view transaction
    viewTransaction({commit, state}, payload){
        return new Promise((resolve, reject) => {
            $axios.get(`/admin/transaction/${payload}`)
            .then((res) => {
                //jika berhasil data yg diperoleh di assign melalui mutation
                commit('VIEW_TRANSACTION', res.data.data)
                resolve(res.data)
            })
        })
    },
    //edit a data transaction
    editTransaction({commit, state}, payload){
        return new Promise((resolve, reject) => {
            $axios.get(`/admin/transaction/${payload}/edit`)
            .then((res) => {
                //jika berhasil data yg diperoleh di assign melalui mutation
                commit('EDIT_TRANSACTION', res.data.data)
                resolve(res.data)
            })
        })
    },
    //update transaction by id. payload passing the id transaction
    updateTransaction({ state, commit }, payload){
        return new Promise((resolve, reject) => {
            commit('SET_LOADING', true)
            $axios.put(`/admin/transaction/${payload}`, state.transaction)
            .then((res) => {
                commit('CLEAR_FORM')
                commit('SET_LOADING', false)
                commit('SET_SUCCESS', res.data, { root: true })
                resolve(res.data)
            })
            .catch((err) => {
                if(err.response.status == 422){
                    commit('SET_ERRORS', err.response.data.errors, {root: true})
                } else if(err.response.status == 400){
                    commit('SET_ERRORS', err.response.data, { root: true })
                }
                commit('SET_LOADING', false)
            })
        })
    },
    //delete a transaction by id
    removeTransaction({ dispatch, commit }, payload){
        return new Promise((resolve, reject) => {
            $axios.delete(`/admin/transaction/${payload}`)
            .then((res) => {
                //res kayaknya tidak berisi data, sehingga tidak perlu dikirm di resolve
                //jika berhasil fecth data terbaru
                dispatch('getTransactions').then(() => resolve())
                commit('SET_SUCCESS', res.data, { root: true })
            })
            .catch((err) => {
                //kirim value error ke store.js
                commit('SET_ERRORS', err.response.data.errors, { root: true })
            })
        })
    },
    //request list umroh package
    getUmrohPackageWithoutPage({ commit, state }, payload){
        //cek apakah di payload ada value search yg dikirmkam
        let search = payload.search
        payload.loading(true) //memberikan loading ke form
        return new Promise((resolve, reject) => {
            //fetch data dari api dengan mengirimkan page dan search
            $axios.get(`/admin/umroh-package/all?q=${search}`)
            .then((res) => {
                //simpan data ke state melalui mutations
                commit('ASSIGN_DATA_UMROH', res.data)
                payload.loading(false)
                resolve(res.data)
            })
        })
    },
}
export default {
    namespaced: true,
    state,
    mutations,
    actions
}
