import $axios from '../../api.js'
import { reject } from 'lodash'

const state = () => ({
    bankAccounts: [], //menampung list data bankAccount dari database. controller index. bentuknya array sehingga banyaknya data dapat dihitung dengan length

    //menampung value dari inputan. datanya dari edit untuk store dan update. controllr store n update
    bankAccount: {
        name: '',
        account_number: '',
        account_name: '',
        branch:'',
        image: '',
    },
    page: 1, //mencatat paginate yg diakses, dg default page 1
    isLoading: false
})

const mutations = {
    //memasukkan ke state bankAccounts untuk ditampilkan (index)
    ASSIGN_DATA(state, payload){
        state.bankAccounts = payload
    },

    SET_PAGE(state, payload){
        state.page = payload
    },

    //mengubah data state bankAccount yg datanya dari edit untuk store n update
    ASSIGN_FORM(state, payload){
        state.bankAccount = {
            name: payload.name,
            account_number: payload.account_number,
            account_name: payload.account_name,
            branch:payload.branch,
            image: payload.image,
        }
    },

    //mengosongkan form
    CLEAR_FORM(state, payload){
        state.bankAccount = {
            name: '',
            account_number: '',
            account_name: '',
            branch:'',
            image: '',
        }
    },
    SET_LOADING(state, payload){
        state.isLoading = payload
    }
}

const actions = {
    //request list bankAccount
    getBankAccounts({ commit, state }, payload){
        //cek apakah di payload ada value search yg dikirmkam
        let search = typeof payload != 'undefined' ? payload : ''
        return new Promise((resolve, reject) => {
            //fetch data dari api dengan mengirimkan page dan search
            $axios.get(`/admin/bank-account?page=${state.page}&q=${search}`)
            .then((res) => {
                //simpan data ke state melalui mutations
                commit('ASSIGN_DATA', res.data)
                resolve(res.data)
            })
        })
    },
    //create a new data
    submitBankAccount({ commit, dispatch, state }){
        let form = new FormData() //mengupload gambar harus pakai formdata
        form.append('name', state.bankAccount.name)
        form.append('account_number', state.bankAccount.account_number)
        form.append('account_name', state.bankAccount.account_name)
        form.append('branch', state.bankAccount.branch)
        form.append('image', state.bankAccount.image)

        return new Promise((resolve, reject) => {
            commit('SET_LOADING', true)
            $axios.post(`/admin/bank-account`, form, {
                //karena terdapat foto maka headernya ditambahkan multipart/form-data
                headers: {
                    'Content-Type' : 'multipart/form-data'
                }
            })
            .then((res) => {
                dispatch('getBankAccounts').then(() => {
                    commit('SET_LOADING', false)
                    resolve(res.data)
                })
                commit('CLEAR_FORM')
                commit('SET_SUCCESS', res.data, { root: true })
            })
            .catch((err) => {
                if(err.response.status == 422){
                    commit('SET_ERRORS', err.response.data.errors, {root: true})
                } else if(err.response.status == 400){
                    commit('SET_ERRORS', err.response.data, { root: true })
                }
                commit('SET_LOADING', false)
            })
        })
    },
    //show a data bankAccount
    editBankAccount({commit, state}, payload){
        return new Promise((resolve, reject) => {
            $axios.get(`/admin/bank-account/${payload}/edit`)
            .then((res) => {
                //jika berhasil data yg diperoleh di assign melalui mutation
                commit('ASSIGN_FORM', res.data.data)
                resolve(res.data)
            })
        })
    },
    //update bankAccount by id. payload passing the id bankAccount
    updateBankAccount({ state, commit }, payload){
        let form = new FormData() //mengupload gambar harus pakai formdata
        form.append('name', state.bankAccount.name)
        form.append('account_number', state.bankAccount.account_number)
        form.append('account_name', state.bankAccount.account_name)
        form.append('branch', state.bankAccount.branch)
        form.append('image', state.bankAccount.image)

        return new Promise((resolve, reject) => {
            commit('SET_LOADING', true)
            $axios.post(`/admin/bank-account/${payload}`, form, {
                //karena terdapat foto maka headernya ditambahkan multipart/form-data
                headers: {
                    'Content-Type' : 'multipart/form-data'
                }
            })
            .then((res) => {
                commit('CLEAR_FORM')
                commit('SET_LOADING', false)
                commit('SET_SUCCESS', res.data, { root: true })
                resolve(res.data)
            })
            .catch((err) => {
                if(err.response.status == 422){
                    commit('SET_ERRORS', err.response.data.errors, {root: true})
                } else if(err.response.status == 400){
                    commit('SET_ERRORS', err.response.data, { root: true })
                }
                commit('SET_LOADING', false)
            })
        })
    },
    //delete a bankAccount by id
    removeBankAccount({ dispatch, commit }, payload){
        return new Promise((resolve, reject) => {
            $axios.delete(`/admin/bank-account/${payload}`)
            .then((res) => {
                //res kayaknya tidak berisi data, sehingga tidak perlu dikirm di resolve
                //jika berhasil fecth data terbaru
                dispatch('getBankAccounts').then(() => resolve())
                commit('SET_SUCCESS', res.data, { root: true })
            })
            .catch((err) => {
                //kirim value error ke store.js
                commit('SET_ERRORS', err.response.data.errors, { root: true })
            })
        })
    }
}
export default {
    namespaced: true,
    state,
    mutations,
    actions
}
