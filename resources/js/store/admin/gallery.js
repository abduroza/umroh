import $axios from '../../api.js'
import { reject } from 'lodash'

const state = () => ({
    galleries: [], //menampung list data gallery dari database. controller index. bentuknya array sehingga banyaknya data dapat dihitung dengan length

    //menampung value dari inputan. datanya dari edit untuk store dan update. controllr store n update
    gallery: {
        image: '',
        umroh_package_id: ''
    },
    page: 1, //mencatat paginate yg diakses, dg default page 1
    isLoading: false
})

const mutations = {
    //memasukkan ke state galleries untuk ditampilkan (index)
    ASSIGN_DATA(state, payload){
        state.galleries = payload
    },

    SET_PAGE(state, payload){
        state.page = payload
    },

    //mengubah data state gallery yg datanya dari edit untuk store n update
    ASSIGN_FORM(state, payload){
        state.gallery = {
            image: payload.image,
            umroh_package_id: payload.umroh_package_id
        }
    },

    //mengosongkan form
    CLEAR_FORM(state, payload){
        state.gallery = {
            image: '',
            umroh_package_id: ''
        }
    },
    SET_LOADING(state, payload){
        state.isLoading = payload
    }
}

const actions = {
    //request list gallery
    getGalleries({ commit, state }, payload){
        //cek apakah di payload ada value search yg dikirmkam
        let search = typeof payload != 'undefined' ? payload : ''
        return new Promise((resolve, reject) => {
            //fetch data dari api dengan mengirimkan page dan search
            $axios.get(`/admin/gallery?page=${state.page}&q=${search}`)
            .then((res) => {
                //simpan data ke state melalui mutations
                commit('ASSIGN_DATA', res.data)
                resolve(res.data)
            })
        })
    },
    //create a new data
    submitGallery({ commit, dispatch, state }){
        let form = new FormData() //mengupload gambar harus pakai formdata
        form.append('image', state.gallery.image)
        form.append('umroh_package_id', state.gallery.umroh_package_id)

        return new Promise((resolve, reject) => {
            commit('SET_LOADING', true)
            $axios.post(`/admin/gallery`, form, {
                //karena terdapat foto maka headernya ditambahkan multipart/form-data
                headers: {
                    'Content-Type' : 'multipart/form-data'
                }
            })
            .then((res) => {
                dispatch('getGalleries').then(() => {
                    commit('SET_LOADING', false)
                    resolve(res.data)
                })
                commit('CLEAR_FORM')
                commit('SET_SUCCESS', res.data, { root: true })
            })
            .catch((err) => {
                if(err.response.status == 422){
                    commit('SET_ERRORS', err.response.data.errors, {root: true})
                } else if(err.response.status == 400){
                    commit('SET_ERRORS', err.response.data, { root: true })
                }
                commit('SET_LOADING', false)
            })
        })
    },
    //show a data gallery
    editGallery({commit, state}, payload){
        return new Promise((resolve, reject) => {
            $axios.get(`/admin/gallery/${payload}/edit`)
            .then((res) => {
                //jika berhasil data yg diperoleh di assign melalui mutation
                commit('ASSIGN_FORM', res.data.data)
                resolve(res.data)
            })
        })
    },
    //update gallery by id. payload passing the id gallery
    updateGallery({ state, commit }, payload){
        let form = new FormData() //mengupload gambar harus pakai formdata
        form.append('image', state.gallery.image)
        form.append('umroh_package_id', state.gallery.umroh_package_id)

        return new Promise((resolve, reject) => {
            commit('SET_LOADING', true)
            $axios.post(`/admin/gallery/${payload}`, form, {
                //karena terdapat foto maka headernya ditambahkan multipart/form-data
                headers: {
                    'Content-Type' : 'multipart/form-data'
                }
            })
            .then((res) => {
                commit('CLEAR_FORM')
                commit('SET_LOADING', false)
                commit('SET_SUCCESS', res.data, { root: true })
                resolve(res.data)
            })
            .catch((err) => {
                if(err.response.status == 422){
                    commit('SET_ERRORS', err.response.data.errors, {root: true})
                } else if(err.response.status == 400){
                    commit('SET_ERRORS', err.response.data, { root: true })
                }
                commit('SET_LOADING', false)
            })
        })
    },
    //delete a gallery by id
    removeGallery({ dispatch, commit }, payload){
        return new Promise((resolve, reject) => {
            $axios.delete(`/admin/gallery/${payload}`)
            .then((res) => {
                //res kayaknya tidak berisi data, sehingga tidak perlu dikirm di resolve
                //jika berhasil fecth data terbaru
                dispatch('getGalleries').then(() => resolve())
                commit('SET_SUCCESS', res.data, { root: true })
            })
            .catch((err) => {
                //kirim value error ke store.js
                commit('SET_ERRORS', err.response.data.errors, { root: true })
            })
        })
    }
}
export default {
    namespaced: true,
    state,
    mutations,
    actions
}
