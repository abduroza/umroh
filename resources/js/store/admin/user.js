import $axios from '../../api.js'
import { reject } from 'lodash'

const state = () => ({
    authenticated: '', //menampung seorang user yg sedang login
    users: [], //menampumg list user
    user: {
        name: '',
        email: '',
        password: '',
        roles: '',
        phone: '',
        birth_date: '',
        gender: '',
        address: '',
        photo: ''
    },
    page: 1, //mencatat page paginate yg sedang diakses
    isLoading: false
})

const mutations = {
    //assign user yg sedang login
    ASSIGN_USER_AUTH(state, payload){
        state.authenticated = payload
    },

    //assign semua user yg ada kecuali user untuk ditampilkan index
    ASSIGN_DATA(state, payload){
        state.users = payload
    },

    //mengubah data state page
    SET_PAGE(state, payload){
        state.page = payload
    },

    //mengubah state user untuk keperluan update yg datanya dari edit
    ASSIGN_FORM(state, payload){
        state.user = {
            name: payload.name,
            email: payload.email,
            password: '',
            roles: payload.roles,
            phone: payload.phone,
            birth_date: payload.birth_date,
            gender: payload.gender,
            address: payload.address,
            photo: ''
        }
    },

    //me-reset state user menjadi kosong
    CLEAR_FORM(state, payload){
        state.user = {
            name: '',
            email: '',
            password: '',
            roles: '',
            phone: '',
            birth_date: '',
            gender: '',
            address: '',
            photo: ''
        }
    },

    SET_LOADING(state, payload){
        state.isLoading = payload
    }
}

const actions = {
    // sudah dipindah di store.js. dibuat global
    getUserLogin({ state, commit }){
        return new Promise((resolve, reject) => {
            $axios.get(`/admin/user-authenticated`)
            .then((res) => {
                commit('ASSIGN_USER_AUTH', res.data.data)
                resolve(res.data)
            })
        })
    },
    getUsers({ commit, state }, payload){ //ini belum dipakai
        let search = typeof payload != 'undefined' ? payload : ''
        return new Promise((resolve, reject) => {
            $axios.get(`/admin/user?page=${state.page}&q=${search}`)
            .then((res) => {
                commit('ASSIGN_DATA', res.data)
                resolve(res.data)
            })
        })
    },
    submitUser({ commit, dispatch, state }){
        let form = new FormData() //mengupload gambar harus pakai formdata
        form.append('name', state.user.name)
        form.append('email', state.user.email)
        form.append('password', state.user.password)
        // form.append('roles', state.user.roles)
        form.append('phone', state.user.phone)
        form.append('birth_date', state.user.birth_date)
        form.append('gender', state.user.gender)
        form.append('address', state.user.address)
        form.append('photo', state.user.photo)

        return new Promise((resolve, reject) => {
            commit('SET_LOADING', true)
            $axios.post(`/admin/user`, form, {
                //karena terdapat foto maka headernya ditambahkan multipart/form-data
                headers: {
                    'Content-Type' : 'multipart/form-data'
                }
            })
            .then((res) => {
                commit('SET_LOADING', false)
                dispatch('getUsers').then(() => {
                    commit('SET_LOADING', false)
                    resolve(res.data)
                })
                commit('SET_SUCCESS', res.data, { root: true })
            })
            .catch((err) => {
                if(err.response.status == 422){
                    commit('SET_ERRORS', err.response.data.errors, {root: true})
                } else if(err.response.status == 400){
                    commit('SET_ERRORS', err.response.data, { root: true })
                }
                commit('SET_LOADING', false)
            })
        })
    },
    //mengambil single data user dari server
    editUser({ commit }, payload){
        return new Promise((resolve, reject) => {
            $axios.get(`/admin/user/${payload}/edit`)
            .then((res) => {
                //Bila berhasil, di assign ke form
                commit('ASSIGN_FORM', res.data.data)
                resolve(res.data)
            })
        })
    },
    //mengupdate data brdasarkan id yg diambil
    updateUser({ commit, state }, payload){

        let form = new FormData() //mengupload gambar harus pakai formdata
        form.append('name', state.user.name)
        form.append('email', state.user.email)
        form.append('password', state.user.password)
        form.append('roles', state.user.roles)
        form.append('phone', state.user.phone)
        form.append('birth_date', state.user.birth_date)
        form.append('gender', state.user.gender)
        form.append('address', state.user.address)
        form.append('photo', state.user.photo)

        return new Promise((resolve, reject) => {
            commit('SET_LOADING', true)
            $axios.post(`/admin/user/${payload}`, form, {
                headers: {
                    'Content-Type' : 'multipart/form-data'
                }
            })
            .then((res) => {
                commit('CLEAR_FORM')
                commit('SET_LOADING', false)
                commit('SET_SUCCESS', res.data, { root: true })
                resolve(res.data)
            })
            .catch((err) => {
                if(err.response.status == 422){
                    commit('SET_ERRORS', err.response.data.errors, {root: true})
                } else if(err.response.status == 400){
                    commit('SET_ERRORS', err.response.data, { root: true })
                }
                commit('SET_LOADING', false)
            })
        })
    },
    removeUser({ dispatch, commit }, payload){
        return new Promise((resolve, reject) => {
            $axios.delete(`/admin/user/${payload}`)
            .then((res) => {
                //jika berhasil, panggil getUsers untuk fetch data dari server
                dispatch('getUsers').then(() => resolve())
                commit('SET_SUCCESS', res.data, { root: true })
            })
            .catch((err) => {
                //kirim value error ke store.js
                commit('SET_ERRORS', err.response.data.errors, { root: true })
            })
        })
    }
}

export default{
    namespaced: true,
    state,
    mutations,
    actions
}
