import $axios from '../../api.js'
import { reject } from 'lodash'

const state = () => ({
    payments: [], //menampung list data payment dari database. controller index. bentuknya array sehingga banyaknya data dapat dihitung dengan length

    //menampung value dari inputan(Form.vue) dan view. untuk store data dan view data
    payment: {
        transaction: '',
        amount: '',
        type: '',
        bank_account: null,
        is_valid: '',
        photo: '',
        date: '',
        user: '',
        // transaction_id: '',
        // bank_account_id: '',
        // user_id: ''
    },
    page: 1, //mencatat paginate yg diakses, dg default page 1
    isLoading: false
})

const mutations = {
    //memasukkan ke state payments untuk ditampilkan (index)
    ASSIGN_DATA(state, payload){
        state.payments = payload
    },

    SET_PAGE(state, payload){
        state.page = payload
    },

    //mengubah data state payment utk view
    VIEW_PAYMENT(state, payload){
        state.payment = //payload
        {
            transaction: payload.transaction,
            amount: payload.amount,
            type: payload.type,
            bank_account: payload.bank_account,
            is_valid: payload.is_valid,
            photo: payload.photo,
            date: payload.date,
            user: payload.user,
            // transaction_id: payload.transaction_id,
            // bank_account_id: payload.bank_account_id,
            // user_id: payload.user_id
        }
    },

    //mengosongkan form
    CLEAR_FORM(state, payload){
        state.payment = {
            transaction: '',
            amount: '',
            type: '',
            bank_account: '',
            is_valid: '',
            photo: '',
            date: '',
            user: '',
            transaction_id: '',
            bank_account_id: '',
            // user_id: ''
        }
    },
    SET_LOADING(state, payload){
        state.isLoading = payload
    }
}

const actions = {
    //request list payment
    getPayments({ commit, state }, payload){
        //cek apakah di payload ada value search yg dikirmkam
        let search = typeof payload.search != 'undefined' ? payload.search : ''
        let is_valid = typeof payload.is_valid != 'undefined' ? payload.is_valid : ''
        return new Promise((resolve, reject) => {
            //fetch data dari api dengan mengirimkan page dan search
            $axios.get(`/payment?page=${state.page}&q=${search}&is_valid=${is_valid}`)
            .then((res) => {
                //simpan data ke state melalui mutations
                commit('ASSIGN_DATA', res.data)
                resolve(res.data)
            })
        })
    },
    //create a new data
    submitPayment({ commit, dispatch, state }, payload){
        let form = new FormData() //mengupload gambar harus pakai formdata
        form.append('amount', state.payment.amount)
        form.append('type', state.payment.type)
        form.append('photo', state.payment.photo)
        form.append('date', state.payment.date)
        form.append('transaction_id', payload)
        form.append('bank_account_id', state.payment.bank_account_id)

        return new Promise((resolve, reject) => {
            commit('SET_LOADING', true)
            $axios.post(`/payment`, form, {
                //karena terdapat foto maka headernya ditambahkan multipart/form-data
                headers: {
                    'Content-Type' : 'multipart/form-data'
                }
            })
            .then((res) => {
                dispatch('getPayments', {search: '', is_valid: 'semua'}).then(() => {
                    commit('SET_LOADING', false)
                    resolve(res.data)
                })
                commit('CLEAR_FORM')
                commit('SET_SUCCESS', res.data, { root: true })
            })
            .catch((err) => {
                if(err.response.status == 422){
                    commit('SET_ERRORS', err.response.data.errors, {root: true})
                } else if(err.response.status == 400){
                    commit('SET_ERRORS', err.response.data, { root: true })
                }
                commit('SET_LOADING', false)
            })
        })
    },
    //show a data payment to view
    viewPayment({commit, state}, payload){
        return new Promise((resolve, reject) => {
            $axios.get(`/admin/payment/${payload}`)
            .then((res) => {
                //jika berhasil data yg diperoleh di assign melalui mutation
                commit('VIEW_PAYMENT', res.data.data)
                resolve(res.data)
            })
        })
    },
    //update to valid by id. payload passing the id payment
    setValid({ state, commit }, payload){
        return new Promise((resolve, reject) => {
            commit('SET_LOADING', true)
            $axios.put(`/admin/payment/valid/${payload}`)
            .then((res) => {
                commit('SET_LOADING', false)
                commit('SET_SUCCESS', res.data, { root: true })
                resolve(res.data)
            })
            .catch((err) => {
                if(err.response.status == 422){
                    commit('SET_ERRORS', err.response.data.errors, {root: true})
                } else if(err.response.status == 400){
                    commit('SET_ERRORS', err.response.data, { root: true })
                }
                commit('SET_LOADING', false)
            })
        })
    },
    //update to not_valid by id. payload passing the id payment
    setInValid({ state, commit }, payload){
        return new Promise((resolve, reject) => {
            commit('SET_LOADING', true)
            $axios.put(`/admin/payment/not-valid/${payload}`)
            .then((res) => {
                commit('SET_LOADING', false)
                commit('SET_SUCCESS', res.data, { root: true })
                resolve(res.data)
            })
            .catch((err) => {
                if(err.response.status == 422){
                    commit('SET_ERRORS', err.response.data.errors, {root: true})
                } else if(err.response.status == 400){
                    commit('SET_ERRORS', err.response.data, { root: true })
                }
                commit('SET_LOADING', false)
            })
        })
    },
}
export default {
    namespaced: true,
    state,
    mutations,
    actions
}
