import { reject } from 'lodash'
import $axios from '../api.js'

const state = () => ({
    //menampilkan semua data
    umrohPackages: {
        title: '',
        slug: '',
        location: '',
        about: '',
        featured_event: '',
        food: '',
        departure_date: '',
        airline: '',
        hotel: '',
        duration: '',
        price: '',
        visa: '',
        galleries: []

    },
})

const mutations = {
    //untuk keperluan index/ get all data
    ASSIGN_DATA(state, payload){
        state.umrohPackages = payload
    },
}

const actions = {
    viewUmrohPackages({commit, state}, payload){
        return new Promise((resolve, reject) => {
            let slug = payload
            $axios.get(`/detail/${slug}`)
            .then((res) => {
                commit('ASSIGN_DATA', res.data.data)
                resolve(res.data)
            })
        })
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}
