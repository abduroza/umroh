import { reject } from 'lodash'
import $axios from '../api.js'

const state = () => ({
    //menampilkan semua data
    umrohPackages: [],
})

const mutations = {
    //untuk keperluan index/ get all data
    ASSIGN_DATA(state, payload){
        state.umrohPackages = payload
    },
}

const actions = {
    getUmrohPackages({commit, state}){
        return new Promise((resolve, reject) => {
            $axios.get(`/`)
            .then((res) => {
                commit('ASSIGN_DATA', res.data.data)
                resolve(res.data)
            })
        })
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}
