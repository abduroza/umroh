import Vue from 'vue'
import Router from 'vue-router'
import store from './store.js'

import Login from './pages/Login.vue'
import Register from './pages/Register.vue'
import Home from './pages/Home.vue'
import Detail from './pages/Detail.vue'
import Checkout from './pages/Checkout.vue'
import Success from './pages/Success.vue'
import Order from './pages/Order.vue'
import Payment from './pages/Payment.vue'
import Bank from './pages/Bank.vue'

import Dashboard from './pages/admin/Dashboard.vue'

import IndexUmrohPackage from './pages/admin/umroh-package/Index.vue'
import DataUmrohPackage from './pages/admin/umroh-package/UmrohPackage.vue'
import AddUmrohPackage from './pages/admin/umroh-package/Add.vue'
import EditUmrohPackage from './pages/admin/umroh-package/Edit.vue'

import IndexGallery from './pages/admin/gallery/Index.vue'
import DataGallery from './pages/admin/gallery/Gallery.vue'
import AddGallery from './pages/admin/gallery/Add.vue'
import EditGallery from './pages/admin/gallery/Edit.vue'

import IndexTransaction from './pages/admin/transaction/Index.vue'
import DataTransaction from './pages/admin/transaction/Transaction.vue'
import ViewTransaction from './pages/admin/transaction/View.vue'
import EditTransaction from './pages/admin/transaction/Edit.vue'

import IndexBankAccount from './pages/admin/bank-account/Index.vue'
import DataBankAccount from './pages/admin/bank-account/BankAccount.vue'
import AddBankAccount from './pages/admin/bank-account/Add.vue'
import EditBankAccount from './pages/admin/bank-account/Edit.vue'

import IndexPayment from './pages/admin/payment/Index.vue'
import DataPayment from './pages/admin/payment/Payment.vue'
import AddPayment from './pages/admin/payment/Add.vue'
import ViewPayment from './pages/admin/payment/View.vue'

import IndexUser from './pages/admin/user/Index.vue'
import DataUser from './pages/admin/user/User.vue'
import AddUser from './pages/admin/user/Add.vue'
import EditUser from './pages/admin/user/Edit.vue'
import home from './store/home.js'


Vue.use(Router)

//DEFINE ROUTE
const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
            // meta: { requiresAuth: true }
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/register',
            name: 'register',
            component: Register
        },
        {
            path: '/detail/:slug',
            name: 'detail',
            component: Detail,
            meta: { title: 'Detail'}
        },
        {
            path: '/checkout/:transaction_id',
            name: 'checkout',
            component: Checkout,
            meta: { requiresAuth: true, title: 'Checkout' },
        },
        {
            path: '/checkout/confirm/:transaction_id',
            name: 'success',
            component: Success,
            meta: { requiresAuth: true, title: 'Success'}
        },
        {
            path: '/order',
            name: 'order',
            component: Order,
            meta: { requiresAuth: true, title: 'Order'}
        },
        {
            path: '/payment/:id',
            name: 'payment',
            component: Payment,
            meta: { requiresAuth: true, title: 'Pembayaran'}
        },
        {
            path: '/bank',
            name: 'bank',
            component: Bank,
            meta: { title: 'Rekening Bank'}
        },
        {
            path: '/admin',
            name: 'dashboard',
            component: Dashboard,
            meta: { requiresAuth: true, isAdmin: true, title: 'Dashboard'}
        },
        {
            path: '/admin/umroh-package',
            component: IndexUmrohPackage,
            meta: { requiresAuth: true, isAdmin: true },
            children: [
                {
                    path: '',
                    name: 'umroh-package.data',
                    component: DataUmrohPackage,
                    meta: { title: 'Paket umroh'},
                },
                {
                    path: 'add',
                    name: 'umroh-package.add',
                    component: AddUmrohPackage,
                    meta: { title: 'Tambah paket umroh'},
                },
                {
                    path: 'edit/:id',
                    name: 'umroh-package.edit',
                    component: EditUmrohPackage,
                    meta: { title: 'Edit paket umroh'},
                },
            ]
        },
        {
            path: '/admin/gallery',
            component: IndexGallery,
            meta: { requiresAuth: true, isAdmin: true },
            children: [
                {
                    path: '',
                    name: 'gallery.data',
                    component: DataGallery,
                    meta: { title: 'Galeri'},
                },
                {
                    path: 'add',
                    name: 'gallery.add',
                    component: AddGallery,
                    meta: { title: 'Tambah galeri'},
                },
                {
                    path: 'edit/:id',
                    name: 'gallery.edit',
                    component: EditGallery,
                    meta: { title: 'Edit Galeri'},
                },
            ]
        },
        {
            path: '/admin/transaction',
            component: IndexTransaction,
            meta: { requiresAuth: true, isAdmin: true },
            children: [
                {
                    path: '',
                    name: 'transaction.data',
                    component: DataTransaction,
                    meta: { title: 'Transaksi'},
                },
                {
                    path: 'view/:id',
                    name: 'transaction.view',
                    component: ViewTransaction,
                    meta: { title: 'View Transaksi'},
                },
                // {
                //     path: 'add',
                //     name: 'transaction.add',
                //     component: AddTransaction,
                //     meta: { title: 'Tambah Transaksi'},
                // },
                {
                    path: 'edit/:id',
                    name: 'transaction.edit',
                    component: EditTransaction,
                    meta: { title: 'Edit Transaksi'},
                },
            ]
        },
        {
            path: '/admin/bank-account',
            component: IndexBankAccount,
            meta: { requiresAuth: true, isAdmin: true },
            children: [
                {
                    path: '',
                    name: 'bank-account.data',
                    component: DataBankAccount,
                    meta: { title: 'Rekening bank'},
                },
                {
                    path: 'add',
                    name: 'bank-account.add',
                    component: AddBankAccount,
                    meta: { title: 'Tambah rekening bank'},
                },
                {
                    path: 'edit/:id',
                    name: 'bank-account.edit',
                    component: EditBankAccount,
                    meta: { title: 'Edit rekening bank'},
                },
            ]
        },
        {
            path: '/admin/payment',
            component: IndexPayment,
            meta: { requiresAuth: true, isAdmin: true },
            children: [
                {
                    path: '',
                    name: 'payment.data',
                    component: DataPayment,
                    meta: { title: 'Pembayaran'},
                },
                {
                    path: 'add/:id',
                    name: 'payment.add',
                    component: AddPayment,
                    meta: { title: 'Upload bukti pembayaran'},
                },
                {
                    path: 'view/:id',
                    name: 'payment.view',
                    component: ViewPayment,
                    meta: { title: 'View Pembayaran'},
                },
            ]
        },
        {
            path: '/admin/user',
            component: IndexUser,
            meta: { requiresAuth: true, isAdmin: true },
            children: [
                {
                    path: '',
                    name: 'user.data',
                    component: DataUser,
                    meta: { title: 'Daftar user'},
                },
                {
                    path: 'add',
                    name: 'user.add',
                    component: AddUser,
                    meta: { title: 'Tambah User'},
                },
                {
                    path: 'edit/:id',
                    name: 'user.edit',
                    component: EditUser,
                    meta: { title: 'Edit User'},
                },
            ]
        },
    ]
});

//Navigation Guards. berfungsi untuk mengecek jika route tersebut membutuhkan proses otentikasi untuk mengakses page-nya, maka dibutuhkan pengecekan lebih lanjut apakah user sudah login atau belum. jika belum secara otomatis akan di-redirect ke route dengan name login, apabila sebaliknya maka akan diteruskan kehalaman yang diinginkan.
router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        store.commit('CLEAR_ERRORS') //Membersihkan state errors setiap kali halaman diload
        // memanggil dari getter isAuth di store.js
        let auth = store.getters.isAuth

        if (!auth) {
            // jika nilai auth dari store.js bernilai null
            next({ name: 'login' })
        } else {
            let userRoles = store.state.userData.roles
            if(to.matched.some(record => record.meta.isAdmin)){
                if(!['0', '1'].includes(userRoles)){
                    next({ name: 'home' })
                } else {
                    next()
                }
            } else {
                next()
            }
        }
    } else {
        next()
    }
})
export default router
