import Vue from 'vue'
import Vuex from 'vuex'

//import module
import auth from './store/auth.js'
import user from './store/admin/user.js'
import home from './store/home.js'
import detail from './store/detail.js'
import checkout from './store/checkout.js'
import order from './store/order.js'
import umrohPackage from './store/admin/umrohPackage.js'
import gallery from './store/admin/gallery.js'
import transaction from './store/admin/transaction.js'
import bankAccount from './store/admin/bankAccount.js'
import payment from './store/admin/payment.js'
import notification from './store/admin/notification.js'
import dashboard from './store/admin/dashboard.js'

Vue.use(Vuex)

//mendefinisikan root store vuex
const store = new Vuex.Store({
    //semua module yg dibuat akan ditempatkan disini
    modules: {
        auth,
        user,
        home,
        detail,
        checkout,
        order,
        umrohPackage,
        gallery,
        transaction,
        bankAccount,
        payment,
        notification,
        dashboard
    },
    //state hampir mirip dengan property data dari component, hanya saja dapat digunakan secara global
    state: {
        //variable token mengmabil dari local storage token
        token: localStorage.getItem('token'),
        userData: JSON.parse(localStorage.getItem('user')),
        success: [],
        errors: [],
    },
    getters: {
        //getter ini akan bernilai true/false. Bernilai true jika tidak null
        isAuth: state => {
            return state.token != 'null' && state.token != null
        }
    },
    mutations: { //mamanipulasi value dari state
        SET_TOKEN(state, payload){
            state.token = payload
        },
        SET_USER_DATA(state, payload){
            state.userData = payload
        },
        SET_SUCCESS(state, payload){
            state.success = payload
        },
        SET_ERRORS(state, payload){
            state.errors = payload
        },
        CLEAR_ERRORS(state, payload){
            state.errors = []
        }
    }
})

export default store
