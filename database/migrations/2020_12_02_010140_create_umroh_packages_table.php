<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUmrohPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('umroh_packages', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('title');
            $table->string('slug');
            $table->string('location')->nullable();
            $table->longText('about');
            $table->string('featured_event')->nullable();
            $table->string('food')->nullable();
            $table->date('departure_date');
            $table->string('airline')->nullable();
            $table->string('hotel')->nullable();
            $table->string('duration');
            $table->integer('price');
            $table->integer('visa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('umroh_packages');
    }
}
