<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('transaction_id');
            $table->date('date');
            $table->integer('amount');
            $table->enum('type', ['transfer', 'cash']);
            $table->uuid('bank_account_id')->nullable();
            $table->enum('is_valid', ['pending', 'valid', 'not_valid'])->default('pending');
            $table->string('photo')->nullable();
            $table->string('umroh_package');
            $table->uuid('user_id');
            $table->timestamps();

            $table->foreign('transaction_id')->references('id')->on('transactions')->onDelete('cascade');
            $table->foreign('bank_account_id')->references('id')->on('bank_accounts')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->dropForeign(['transaction_id']);
            $table->dropForeign(['bank_account_id']);
            $table->dropForeign(['user_id']);
        });
        Schema::dropIfExists('payments');
    }
}
