<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\UmrohPackage;
use Carbon\Carbon;

class UmrohPackagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UmrohPackage::create(
            [
                'title' => 'Umroh Plus Turki',
                'slug' => 'umroh-plus-turki',
                'location' => 'Makkah',
                'about' => 'Perjalanan umroh kemjudian lanjut ke Turki',
                'featured_event' => 'Kunjungan ke Hagia Sophia',
                'food' => 'Turki food',
                'departure_date' => Carbon::now()->addMonths(2),
                'airline' => 'Garuda',
                'hotel' => 'Mekkah Tower',
                'duration' => '10',
                'price' => 3100,
                'visa' => 200
            ]
        );

        UmrohPackage::create(
            [
                'title' => 'Umroh Plus Dubai',
                'slug' => 'umroh-plus-dubai',
                'location' => 'Makkah',
                'about' => 'Perjalanan umroh kemjudian lanjut ke Dubai Uni Emirates Arab',
                'featured_event' => 'Kunjungan ke Dubai',
                'food' => 'UEA food',
                'departure_date' => Carbon::now()->addMonths(3),
                'airline' => 'Garuda',
                'hotel' => 'Mekkah Tower',
                'duration' => '8',
                'price' => 2800,
                'visa' => 180
            ],
        );
    }
}
