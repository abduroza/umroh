<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(
            [
                'name' => 'Admin',
                'email' => 'admin@gmail.com',
                'email_verified_at' => now(),
                'password' => bcrypt('12345'),
                'roles' => 0,
                'username' => 'admin',
                'phone' => '085647701900',
                'gender' => true,
                'birth_date' => now(),
                'address' => 'Walen, Simo, Boyolali'
            ]
        );

        User::create(
            [
                'name' => 'Kiman',
                'email' => 'kiman@gmail.com',
                'email_verified_at' => now(),
                'password' => bcrypt('12345'),
                'roles' => 1,
                'username' => 'kiman',
                'phone' => '085783267836',
                'gender' => true,
                'birth_date' => now(),
                'address' => 'Nogosari, Boyolali'
            ]
        );

        User::create(
            [
                'name' => 'Kijo',
                'email' => 'kijo@gmail.com',
                'email_verified_at' => now(),
                'password' => bcrypt('12345'),
                'roles' => 1,
                'username' => 'kijo',
                'phone' => '08537834893',
                'gender' => false,
                'birth_date' => now(),
                'address' => 'Banjarsari, Surakarta'
            ]
        );

        User::create(
            [
                'name' => 'Enthong',
                'email' => 'enthong@gmail.com',
                'email_verified_at' => now(),
                'password' => bcrypt('12345'),
                'roles' => 1,
                'username' => 'enthong',
                'phone' => '085789237232',
                'gender' => true,
                'birth_date' => now(),
                'address' => 'Gunungmadu, Simo, Boyolali'
            ]
        );

        User::create(
            [
                'name' => 'Uyeng',
                'email' => 'uyeng@gmail.com',
                'email_verified_at' => now(),
                'password' => bcrypt('12345'),
                'roles' => 1,
                'username' => 'uyeng',
                'phone' => '08537864893',
                'gender' => false,
                'birth_date' => now(),
                'address' => 'Gunungmadu, Simo, Boyolali'
            ],
        );


    }
}
