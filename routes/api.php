<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::post('/register', 'Auth\LoginController@register');

Route::group(['namespace' => 'API'], function(){
    Route::get('/', 'HomeController@index');
    Route::get('/detail/{slug}', 'DetailController@index');

    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('/checkout/{transaction_id}', 'CheckoutController@index');
        Route::post('/checkout/{umroh_package_id}', 'CheckoutController@addToCart');
        Route::post('/checkout/add-member/{transaction_id}', 'CheckoutController@addMember');
        Route::delete('/checkout/remove-member/{transaction_detail_id}', 'CheckoutController@removeMember');
        Route::delete('/checkout/{transaction_id}', 'CheckoutController@cancelBooking');
        Route::put('/checkout/confirm/{transaction_id}', 'CheckoutController@success');

        Route::get('/order', 'OrderController@index');

        Route::get('/admin/bank-account', 'BankAccountController@index');
        Route::get('/payment', 'PaymentController@index');
        Route::post('/payment', 'PaymentController@store');
        Route::get('/admin/user-authenticated', 'UserController@getUserLogin');

        Route::get('/notification', 'NotificationController@index');
        Route::post('/notification', 'NotificationController@store');

        Route::group(['prefix' => 'admin', 'middleware' => ['checkRole:0,1'] ], function() {
            Route::get('/umroh-package', 'UmrohPackageController@index');
            Route::get('/umroh-package/all', 'UmrohPackageController@indexAll');
            Route::post('/umroh-package', 'UmrohPackageController@store');
            Route::get('/umroh-package/{id}/edit', 'UmrohPackageController@edit');
            Route::put('/umroh-package/{id}', 'UmrohPackageController@update');
            Route::delete('/umroh-package/{id}', 'UmrohPackageController@destroy');

            Route::get('/gallery', 'GalleryController@index');
            Route::post('/gallery', 'GalleryController@store');
            Route::get('/gallery/{id}/edit', 'GalleryController@edit');
            Route::post('/gallery/{id}', 'GalleryController@update');
            Route::delete('/gallery/{id}', 'GalleryController@destroy');

            Route::get('/transaction', 'TransactionController@index');
            Route::post('/transaction', 'TransactionController@store');
            Route::get('/transaction/{id}', 'TransactionController@view');
            Route::get('/transaction/{id}/edit', 'TransactionController@edit');
            Route::put('/transaction/{id}', 'TransactionController@update');
            Route::delete('/transaction/{id}', 'TransactionController@destroy');

            Route::get('/payment/{id}', 'PaymentController@view');
            Route::put('/payment/valid/{id}', 'PaymentController@valid');
            Route::put('/payment/not-valid/{id}', 'PaymentController@notValid');

            Route::get('/user', 'UserController@index');
            Route::post('/user', 'UserController@store');
            Route::get('/user/{id}/edit', 'UserController@edit');
            Route::post('/user/{id}', 'UserController@update');
            Route::delete('/user/{id}', 'UserController@destroy');

            Route::group(['middleware' => ['checkRole:0'] ], function(){
                Route::post('/bank-account', 'BankAccountController@store');
                Route::get('/bank-account/{id}/edit', 'BankAccountController@edit');
                Route::post('/bank-account/{id}', 'BankAccountController@update');
                // Route::delete('/bank-account/{id}', 'BankAccountController@destroy');
            });

            Route::get('/chart', 'DashboardController@chart');
            Route::get('/export', 'DashboardController@exportData');

        });

              
        

    });

});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
