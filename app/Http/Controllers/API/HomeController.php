<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\UmrohPackage;

class HomeController extends Controller 
{
    public function index(Request $request)
    {

        $umroh_packages = UmrohPackage::with(['galleries'])->get();

        return response()->json(['status' => 'success', 'data' => $umroh_packages]);
    }
}