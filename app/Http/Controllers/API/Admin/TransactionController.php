<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{Transaction, TransactionDetail, UmrohPackage, User};
use App\Http\Resources\TransactionCollection; //untuk men-format data dari hasil query yang telah dilakukan, dimana data yang diambil akan kita batas hanya 10 data dalam sekali query menggunakan fungsi paginate()
use Illuminate\Support\Str;
use DB;
use File;

class TransactionController extends Controller
{
    public function index()
    {
        $transactions = Transaction::with(['transaction_details', 'umroh_package', 'user', 'payments.bank_account'])->orderBy('created_at', 'DESC');
        $search = request()->q;
        $status = request()->status;

        if($search != ''){
            $transactions = $transactions->whereHas('user', function($query) use ($search) {
                    $query->where('name', 'LIKE', '%'.$search.'%')
                        ->orWhere('address', 'LIKE', '%'.$search.'%')
                        ->orWhere('email', 'LIKE', '%'.$search.'%');
                });
        }

        if(in_array($status, [0,1,2,3,4,5])){
            //maka ambil data berdasarkan status tersebut
            $transactions = $transactions->where('status', $status);
        }

        $transactions = $transactions->paginate(10);

        return new TransactionCollection($transactions);
    }

    public function view($id)
    {
        $transaction = Transaction::with(['user', 'umroh_package', 'transaction_details.user', 'payments'])->findOrFail($id);

        return response()->json(['status' => 'success', 'data' => $transaction, 'message' => 'Berhasil menampilkan transaksi'], 200);
    }

    public function edit($id)
    {
        $transaction = Transaction::with(['transaction_details.user', 'umroh_package'])->findOrFail($id);

        return response()->json(['status' => 'success', 'data' => $transaction, 'message' => 'Berhasil menampilkan transaksi'], 200);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'umroh_package' => 'required',
            'transaction_details' => 'required',
            'transaction_details.*.user.email' => 'required|exists:users,email',
            'transaction_details.*.is_visa' => 'required|boolean',
            'transaction_details.*.doe_passport' => 'required',
        ],[
            'required' => 'Wajib diisi',
            'exists' => 'Tidak ditemukan / terdaftar'
        ]);

        DB::beginTransaction();
        try {
            $transaction = Transaction::findOrFail($id);
            $umroh_package = UmrohPackage::findOrFail($request['umroh_package']['id']);
            $umroh_price = 0;
            $additional_visa = 0;

            //menghapus transaction_detailnya dulu
            TransactionDetail::whereIn('transaction_id', [$id])->delete();

            foreach ($request->transaction_details as $detail) {
                if(!is_null($detail['user'])){
                    $user = User::where('email', $detail['user']['email'])->first();

                    //membuat transaction detail. karena transaction detail sebelumnya sudah dihapus
                    TransactionDetail::create([
                        'transaction_id' => $id,
                        'user_id' => $user->id,
                        'is_visa' => $detail['is_visa'],
                        'doe_passport' => $detail['doe_passport']
                    ]);

                    if ($detail['is_visa'] == true) {
                        $additional_visa += $umroh_package->visa;
                        $umroh_price += $umroh_package->price;
                    }
                }
            }

            $transaction->update([
                'umroh_package_id' => $request['umroh_package']['id'],
                'additional_visa' => $additional_visa,
                'total' => $umroh_price + $additional_visa,
            ]);
            
            $transaction->load('transaction_details'); //supaya response yg dikirim juga menyertakan table transaction_details. relationship transaction_details harus dibuat dulu di model Transaction.php
            
            DB::commit();
            return response()->json(['status' => 'success', 'data' => $transaction, 'message' => 'Berhasil mengupdate transaksi'], 201);
        } catch (\Exception $err) {
            DB::rollback();
            return response()->json(['status' => 'errors', 'message' => $err->getMessage()], 400);
        }
    }




}
