<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{Gallery, UmrohPackage};
use App\Http\Resources\GalleryCollection; //untuk men-format data dari hasil query yang telah dilakukan, dimana data yang diambil akan kita batas hanya 10 data dalam sekali query menggunakan fungsi paginate()
use Illuminate\Support\Str;
use DB;
use File;

class GalleryController extends Controller
{
    public function index()
    {
        $galleries = Gallery::with(['umroh_package'])->orderBy('created_at', 'DESC');
        $input = request()->q;

        if($input != ''){
            $galleries = $galleries->WhereHas('umroh_package', function($query) use ($input) {
                    return $query->where('title', 'LIKE', '%'.$input.'%');
                });
        }
        $galleries = $galleries->paginate(10);

        return new GalleryCollection($galleries);
    }

    public function store(Request $request)
    {
        $request->validate([
            'image' => 'required|image',
            'umroh_package_id' => 'required|exists:umroh_packages,id'
        ]);

        DB::beginTransaction();
        try {
            $umroh_package = UmrohPackage::findOrFail($request->umroh_package_id);

            $image_name = null;

            if($request->hasFile('image')){
                $file = $request->file('image');
                $image_name = $umroh_package->slug . '-' . time() . '.' . $file->getClientOriginalExtension();
                $file->storeAs('public/galleries', $image_name); //file disimpam di storage/app/public/galleries
            }

            $gallery = Gallery::create([
                'image' => $image_name,
                'umroh_package_id' => $request->umroh_package_id
            ]);

            DB::commit();
            return response()->json(['status' => 'success', 'data' => $gallery, 'message' => 'Berhasil menambahkan galeri baru'], 201);
        } catch (\Exception $err) {
            DB::rollback();
            return response()->json(['status' => 'error', 'message' => $err->getMessage()], 400);
        }
    }

    public function edit($id)
    {
        $gallery = Gallery::with(['umroh_package'])->findOrFail($id);

        return response()->json(['status' => 'success', 'data' => $gallery, 'message' => 'Berhasil menampilkan galeri'], 200);
    }

    public function update(Request $request, $id)
    {
        
        $request->validate([
            'image' => 'nullable|image',
            'umroh_package_id' => 'required|exists:umroh_packages,id'
        ]);
        
        DB::beginTransaction();
        try {
            $gallery = Gallery::findOrFail($id);
            $umroh_package = UmrohPackage::findOrFail($request->umroh_package_id);

            
            $image_name = $gallery->image;
            if($request->hasFile('image')){
                $file = $request->file('image');
                // $image_name ? File::delete(public_path('app/public/galleries/' . $image_name)) : ''; //hapus foto lama jika punya. jika foto disimpan di folder public bukan storage
                $image_name ? File::delete(storage_path('app/public/galleries/' . $image_name)) : ''; //hapus foto lama jika ada
                $image_name = $umroh_package->slug . '-' . time() . '.' . $file->getClientOriginalExtension();
                $file->storeAs('public/galleries', $image_name); //file disimpam di storage/app/public/galleries
            }

            $gallery->update([
                'image' => $image_name,
                'umroh_package_id' => $request->umroh_package_id
            ]);

            DB::commit();
            return response()->json(['status' => 'success', 'data' => $gallery, 'message' => 'Berhasil mengupdate galeri'], 200);
        } catch (\Exception $err) {
            DB::rollback();
            return response()->json(['status' => 'error', 'message' => $err->getMessage()], 400);
        }
    }

    public function destroy($id)
    {
        $gallery = Gallery::findOrFail($id);

        $image_name = $gallery->image;
        $image_name ? File::delete(storage_path('app/public/galleries/' . $image_name)) : '';

        $gallery->delete();

        return response()->json(['status' => 'success', 'data' => $gallery, 'message' => 'Berhasil menghapus galeri'], 200);
    }
}
