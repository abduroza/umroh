<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{BankAccount};
use App\Http\Resources\BankAccountCollection;
use Illuminate\Support\Str;
use DB;
use File;

class BankAccountController extends Controller
{
    public function index()
    {
        $bank_accounts = BankAccount::orderBy('created_at', 'DESC');
        $search = request()->q;

        if($search != ''){
            $bank_accounts = $bank_accounts->where('name', 'LIKE', '%'.$search.'%');
        }

        $bank_accounts = $bank_accounts->paginate(10);

        return new BankAccountCollection($bank_accounts);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'account_number' => 'required|string',
            'account_name' => 'required|string',
            'branch' => 'required|string',
            // 'image' => 'nullable|image',
        ]);

        DB::beginTransaction();
        try {
            $image_name = null;

            if($request->hasFile('image')){
                $file = $request->file('image');
                $image_name = Str::slug($request->name) . '-' . time() . '.' . $file->getClientOriginalExtension();
                $file->storeAs('public/bank_accounts', $image_name); //file disimpam di storage/app/public/bank_accounts
            }

            $bank_account = BankAccount::create([
                'name' => $request->name,
                'account_number' => $request->account_number,
                'account_name' => $request->account_name,
                'branch' => $request->branch,
                'image' => $image_name
            ]);

            DB::commit();
            return response()->json(['status' => 'success', 'data' => $bank_account, 'message' => 'Berhasil menambahkan rekening bank baru'], 201);
        } catch (\Exception $err) {
            DB::rollback();
            return response()->json(['status' => 'error', 'message' => $err->getMessage()], 400);
        }
    }

    public function edit($id)
    {
        $bank_account = BankAccount::findOrFail($id);

        return response()->json(['status' => 'success', 'data' => $bank_account, 'message' => 'Berhasil menampilkan rekening bank'], 200);
    }

    public function update(Request $request, $id)
    {
        
        $request->validate([
            'name' => 'required|string',
            'account_number' => 'required|string',
            'account_name' => 'required|string',
            'branch' => 'required|string',
            // 'image' => 'image|nullable',
        ]);
        
        DB::beginTransaction();
        try {
            $bank_account = BankAccount::findOrFail($id);
            
            $image_name = $bank_account->image;
            if($request->hasFile('image')){
                $file = $request->file('image');
                // $image_name ? File::delete(public_path('app/public/bank_accounts/' . $image_name)) : ''; //hapus foto lama jika punya. jika foto disimpan di folder public bukan storage
                $image_name ? File::delete(storage_path('app/public/bank_accounts/' . $image_name)) : ''; //hapus foto lama jika ada
                $image_name = Str::slug($request->name) . '-' . time() . '.' . $file->getClientOriginalExtension();
                $file->storeAs('public/bank_accounts', $image_name); //file disimpam di storage/app/public/bank_accounts
            }

            $bank_account->update([
                'name' => $request->name,
                'account_number' => $request->account_number,
                'account_name' => $request->account_name,
                'branch' => $request->branch,
                'image' => $image_name
            ]);

            DB::commit();
            return response()->json(['status' => 'success', 'data' => $bank_account, 'message' => 'Berhasil mengupdate rekening bank'], 200);
        } catch (\Exception $err) {
            DB::rollback();
            return response()->json(['status' => 'error', 'message' => $err->getMessage()], 400);
        }
    }

    // public function destroy($id)
    // {
    //     $bank_account = BankAccount::findOrFail($id);

    //     $image_name = $bank_account->image;
    //     $image_name ? File::delete(storage_path('app/public/bank_accounts/' . $image_name)) : '';

    //     $bank_account->delete();

    //     return response()->json(['status' => 'success', 'data' => $bank_account, 'message' => 'Berhasil menghapus rekening bank'], 200);
    // }
}
