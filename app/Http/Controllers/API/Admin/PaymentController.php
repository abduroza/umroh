<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{Payment, User, Transaction};
use App\Http\Resources\PaymentCollection;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use App\Notifications\PaymentNotification;
use DB;
use File;

class PaymentController extends Controller
{
    public function index()
    {
        $payments = Payment::with(['transaction.umroh_package', 'transaction.transaction_details', 'bank_account', 'user'])->orderBy('created_at', 'DESC');
        $search = request()->q;
        $is_valid = request()->is_valid;

        if($search != ''){
            $payments = $payments->where('amount', 'LIKE', '%'.$search.'%')
                ->orWhereHas('user', function($query) use ($search) {
                    $query->where('name', 'LIKE', '%'.$search.'%')
                        ->orWhere('phone', 'LIKE', '%'.$search.'%')
                        ->orWhere('address', 'LIKE', '%'.$search.'%');
                });
        }

        if(in_array($is_valid, ['pending', 'valid', 'not_valid'])){
            //maka ambil data berdasarkan status tersebut
            $payments = $payments->where('is_valid', $is_valid);
        }

        $payments = $payments->paginate(100);

        return new PaymentCollection($payments);
    }

    public function store(Request $request)
    {
        $request->validate([
            'transaction_id' => 'required|exists:transactions,id',
            'date' => 'required',
            'amount' => 'required|integer',
            'type' => 'required|string',
            'bank_account_id' => 'nullable',
            'photo' => 'required|image',
        ]);

        DB::beginTransaction();
        try {
            $user = Auth::user();
            $transaction = Transaction::with(['payments', 'umroh_package'])->findOrFail($request->transaction_id);

            $paid = $request->amount;
            foreach ($transaction->payments as $payment) {
                if ($payment->is_valid == 'valid') {
                    $paid += $payment->amount;
                }
            }
            
            $image_name = null;
            if($request->hasFile('photo')){
                $file = $request->file('photo');
                $image_name = $request->transaction_id . '-' . time() . '.' . $file->getClientOriginalExtension();
                $file->storeAs('public/payments', $image_name); //file disimpam di storage/app/public/payments
            }

            $is_valid = $user->roles == '0' || $user->roles == '1' ? 'valid' : 'pending';
            $bank_account = $request->type == 'cash' ? null : $request->bank_account_id;
            $payment = Payment::create([
                'transaction_id' => $request->transaction_id,
                'date' => $request->date,
                'amount' => $request->amount,
                'type' => $request->type,
                'bank_account_id' => $bank_account,
                'is_valid' => $is_valid, //jika yg login admin, is_valid di set valid
                'photo' => $image_name,
                // 'umroh_package' => $transaction->umroh_package->title,
                'user_id' => $user->id
            ]);

            if($is_valid == 'valid' && $transaction->total <= $paid) {
                $transaction->status = 2; //2 berarti success
                $transaction->save();
            }

            $paymentNotification = new PaymentNotification($payment, $user);

            //mengambil semua user dg role superadmin dan admin. dua jenis user inilah yg akan menerima notifikasi
            $users = User::whereIn('roles', [0, 1])->get();

            //mengirim notifikasinya menggunakan facade notification
            Notification::send($users, $paymentNotification);

            DB::commit();
            return response()->json(['status' => 'success', 'data' => $payment, 'message' => 'Berhasil menambahkan pembayaran baru'], 201);
        } catch (\Exception $err) {
            DB::rollback();
            return response()->json(['status' => 'error', 'message' => $err->getMessage()], 400);
        }
    }

    public function view($id)
    {
        $payment = Payment::with(['transaction.umroh_package', 'transaction.transaction_details', 'bank_account', 'user'])->findOrFail($id);

        return response()->json(['status' => 'success', 'data' => $payment, 'message' => 'Berhasil menampilkan pembayaran'], 200);
    }

    // untuk meng-accept pembayaran dari user
    public function valid($id)
    {
        DB::beginTransaction();
        try {
            $payment = Payment::findOrFail($id);

            $payment->update([
                'is_valid' => 'valid'
            ]);

            $transaction = Transaction::with(['payments', 'user'])->findOrFail($payment->transaction_id);

            $paid = 0;
            foreach ($transaction->payments as $payment) {
                if($payment->is_valid == 'valid'){
                    $paid += $payment->amount;
                }
            }

            if($transaction->total <= $paid){
                $transaction->status = 2; //2 berarti success
                $transaction->save();
            }

            $paymentNotification = new PaymentNotification($payment, $transaction->user);
            //mengirim notifikasinya menggunakan facade notification
            Notification::send($transaction->user, $paymentNotification); //notifikasinya dikirim ke user yg membayar

            DB::commit();
            return response()->json(['status' => 'success', 'data' => $payment, 'message' => 'Berhasil menerima pembayaran'], 200);
        } catch (\Exception $err) {
            DB::rollback();
            return response()->json(['status' => 'error', 'message' => $err->getMessage()], 400);
        }
    }

    // untuk me-reject pembayaran dari user
    public function notValid($id)
    {
        DB::beginTransaction();
        try {
            $payment = Payment::findOrFail($id);

            $payment->update([
                'is_valid' => 'not_valid'
            ]);

            $transaction = Transaction::with(['user'])->findOrFail($payment->transaction_id);

            $paymentNotification = new PaymentNotification($payment, $transaction->user);
            //mengirim notifikasinya menggunakan facade notification
            Notification::send($transaction->user, $paymentNotification); //notifikasinya dikirim ke user yg membayar

            DB::commit();
            return response()->json(['status' => 'success', 'data' => $payment, 'message' => 'Berhasil me-reject pembayaran'], 200);
        } catch (\Exception $err) {
            DB::rollback();
            return response()->json(['status' => 'error', 'message' => $err->getMessage()], 400);
        }
    }

    // public function destroy($id)
    // {
    //     $payment = Payment::findOrFail($id);

    //     $image_name = $payment->image;
    //     $image_name ? File::delete(storage_path('app/public/payments/' . $image_name)) : '';

    //     $payment->delete();

    //     return response()->json(['status' => 'success', 'data' => $payment, 'message' => 'Berhasil menghapus pembayaran'], 200);
    // }
}
