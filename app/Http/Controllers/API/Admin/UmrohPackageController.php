<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UmrohPackage;
use App\Http\Resources\UmrohPackageCollection; //untuk men-format data dari hasil query yang telah dilakukan, dimana data yang diambil akan kita batas hanya 10 data dalam sekali query menggunakan fungsi paginate()
use Illuminate\Support\Str;

class UmrohPackageController extends Controller
{
    public function index()
    {
        $umroh_packages = UmrohPackage::orderBy('created_at', 'DESC');;
        if(request()->q != ''){
            $umroh_packages = $umroh_packages->where('title', 'LIKE', '%'.request()->q.'%')
                ->orWhere('hotel', 'LIKE', '%'.request()->q.'%')
                ->orWhere('airline', 'LIKE', '%'.request()->q.'%');
        }
        $umroh_packages = $umroh_packages->paginate(8);

        return new UmrohPackageCollection($umroh_packages);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string|unique:umroh_packages,title',
            'location' => 'string',
            'about' => 'string|required',
            'featured_event' => 'string|nullable',
            'food' => 'string|nullable',
            'departure_date' => 'required',
            'airline' => 'string|nullable',
            'hotel' => 'string|nullable',
            'duration' => 'string|nullable',
            'price' => 'numeric|required',
            'visa' => 'numeric|required'
        ]);

        $umroh_package = Umrohpackage::create([
            'title' => $request->title,
            'slug' => Str::slug($request->title),
            'location' => $request->location,
            'about' => $request->about,
            'featured_event' => $request->featured_event,
            'food' => $request->food,
            'departure_date' => $request->departure_date,
            'airline' => $request->airline,
            'hotel' => $request->hotel,
            'duration' => $request->duration,
            'price' => $request->price,
            'visa' => $request->visa
        ]);

        return response()->json(['status' => 'success', 'data' => $umroh_package, 'message' => 'Berhasil membuat paket umroh'], 201);
    }

    public function edit($id)
    {
        $umroh_package = UmrohPackage::findOrFail($id);

        return response()->json(['status' => 'success', 'data' => $umroh_package, 'message' => 'Berhasil menampilkan paket umroh'], 200);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|string',
            'location' => 'string',
            'about' => 'string|required',
            'featured_event' => 'string|nullable',
            'food' => 'string|nullable',
            'departure_date' => 'required',
            'airline' => 'string|nullable',
            'hotel' => 'string|nullable',
            'duration' => 'string|nullable',
            'price' => 'numeric|required',
            'visa' => 'numeric|required',
        ]);

        $data = $request->all();
        $data['slug'] = Str::slug($request->title);
        
        $umroh_package = UmrohPackage::findOrFail($id);

        $umroh_package->update($data);

        return response()->json(['status' => 'success', 'data' => $umroh_package, 'message' => 'Berhasil mengupdate paket umroh'], 200);
    }

    public function destroy($id)
    {
        $umroh_package = UmrohPackage::findOrFail($id);

        $umroh_package->delete();

        return response()->json(['status' => 'success', 'data' => $umroh_package, 'message' => 'Berhasil menghapus paket umroh'], 200);
    }

    public function indexAll()
    {
        $umroh_packages = UmrohPackage::orderBy('created_at', 'DESC');;
        if(request()->q != ''){
            $umroh_packages = $umroh_packages->where('title', 'LIKE', '%'.request()->q.'%')
                ->orWhere('hotel', 'LIKE', '%'.request()->q.'%')
                ->orWhere('airline', 'LIKE', '%'.request()->q.'%');
        }

        $umroh_packages = $umroh_packages->get();

        return response()->json(['status' => 'success', 'data' => $umroh_packages, 'message' => 'Berhasil menampilkan semua paket umroh'], 200);
    }
}
