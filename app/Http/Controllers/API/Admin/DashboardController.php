<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Transaction;
use Carbon\Carbon;
use DB;

// export excel
use App\Exports\TransactionsExport;
use Maatwebsite\Excel\Facades\Excel;

class DashboardController extends Controller
{
    public function chart(Request $request)
    {
        $filter = $request->year . '-' . $request->month; //request data bulan dan tahun dari query params "/chart?month=...&year=..." contoh output "2021-03"

        $parse = Carbon::parse($filter); //ubah formatnya jadi format carbon. cth "2020-08-31T23:59:59.999999Z"

        //membuat range tanggal dari tanggal 1 - 31 atau menyesuaikan jumlah hari dalm satu bulan
        $array_date = range($parse->startOfMonth()->format('d'), $parse->endOfMonth()->format('d'));

        //get data transaksi berdasarkan bulan dan tanggal yg diminta, kemudian kelompokkan berdasrkan tanggalnya. sum data total kemudian simpan ke nama baru yaitu total
        // date(created_at) as date berarti mengambil date di field created_at. sum(total) as total berarti menjumlahkan field total dalam satu hari
        $orders = Transaction::select(DB::raw('date(created_at) as date, sum(total) as total'))
                    ->where('created_at', 'LIKE', '%'.$filter.'%')
                    ->groupBy(DB::raw('date(created_at)'))->get();

        // dd($orders);
        $data = [];
        //looping tanggal bulan saat ini
        foreach($array_date as $row){
            //cek tanggalnya, jika hanya 1 angka (1-9) maka tambahkan angka 0 didepannya. misal 01, 02, 03 dst
            $f_date = strlen($row) == 1 ? 0 . $row : $row;

            $datee = $filter . '-' . $f_date; //menambahkan tanggal. yg awalnya "2020-07" sehingga jadi "2020-07-02"

            //cari data date berdasrkan $datee pada collection hasil query
            $totall = $orders->firstWhere('date', $datee);

            //simpan data ke variable data
            $data[] = [
                'date' => $datee,
                'total' => $totall ? $totall->total : 0 //$total->total berarti mengambil dari "sum(total) as total". akan menampilkan jumlah total omset selama sehari
            ];
        }

        return $data;
    }
    
    public function exportData(Request $request)
    {
        $transactions = $this->chart($request); //berisi data chart. data chart berisi date dan total amount order
        $fileName = 'TransactionsUmroh' . Carbon::now()->setTimezone('Asia/Jakarta')->format('d-m-Y H:i') . '.xlsx'; //TransactionsUmroh04-07-2020 16_12.xlsx

        //Memanggil TransactionsExport dengan mengirimkan data transaksi, month, year.$request->month mengarah ke Dashboard.vue pada method exportData(). Mengambil query params
        return Excel::download(new TransactionsExport($transactions, $request->month, $request->year), $fileName);
    }
}
