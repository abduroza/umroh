<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\UserCollection;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\Models\{User};
use File;
use DB;

class UserController extends Controller
{
    public function index()
    {
        $users = User::orderBy('name', 'ASC');
        $search = request()->q;

        $user = request()->user(); //get user yg sedang login
        $user->roles == '1' ? $users = $users->where('roles', 2) : '';

        if($search != ''){
            $users = $users->where('name', 'LIKE', '%'.$search.'%')
                ->orWhere('phone', 'LIKE', '%'.$search.'%')
                ->orWhere('email', 'LIKE', '%'.$search.'%');
        }

        $users = $users->paginate(100);

        return new UserCollection($users);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|unique:users,email',
            'password' => 'required|string',
            'phone' => 'required|string|min:9',
            'birth_date' => 'required|string',
            'gender' => 'required|boolean',
            'address' => 'required|string',
            'photo' => 'nullable|image',
        ]);

        DB::beginTransaction();
        try {
            $image_name = null;

            if($request->hasFile('photo')){
                $file = $request->file('photo');
                $image_name = Str::slug($request->name) . '-' . time() . '.' . $file->getClientOriginalExtension();
                $file->storeAs('public/users', $image_name); //file disimpam di storage/app/public/users
            }

            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'roles' => 2,
                'phone' => $request->phone,
                'birth_date' => $request->birth_date,
                'gender' => $request->gender,
                'address' => $request->address,
                'photo' => $image_name,
            ]);

            DB::commit();
            return response()->json(['status' => 'success', 'data' => $user, 'message' => 'Berhasil menambahkan customer baru'], 201);
        } catch (\Exception $err) {
            DB::rollback();
            return response()->json(['status' => 'error', 'message' => $err->getMessage()], 400);
        }
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);

        return response()->json(['status' => 'success', 'data' => $user, 'message' => 'Berhasil menampilkan user'], 200);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string',
            'password' => 'nullable',
            'phone' => 'required|string|min:9',
            'birth_date' => 'required|string',
            'gender' => 'required|boolean',
            'address' => 'required|string',
            'photo' => 'nullable',
        ]);
        
        DB::beginTransaction();
        try {
            $user = User::findOrFail($id);
            $userLogin = request()->user(); //get user yg sedang login

            $image_name = $user->photo;
            if($request->hasFile('photo')){
                $file = $request->file('photo');
                // $image_name ? File::delete(public_path('app/public/users/' . $image_name)) : ''; //hapus foto lama jika punya. jika foto disimpan di folder public bukan storage
                $image_name ? File::delete(storage_path('app/public/users/' . $image_name)) : ''; //hapus foto lama jika ada
                $image_name = Str::slug($request->name) . '-' . time() . '.' . $file->getClientOriginalExtension();
                $file->storeAs('public/users', $image_name); //file disimpam di storage/app/public/users
            }

            $password = $request->password != null ? bcrypt($request->password) : $user->password;

            $roles = $userLogin->roles != 0 ? $user->roles : $request->roles;
            $user->update([
                'name' => $request->name,
                'email' => $request->email,
                'password' => $password,
                'roles' => $roles,
                'phone' => $request->phone,
                'birth_date' => $request->birth_date,
                'gender' => $request->gender,
                'address' => $request->address,
                'photo' => $image_name,
            ]);

            DB::commit();
            return response()->json(['status' => 'success', 'data' => $user, 'message' => 'Berhasil mengupdate user'], 200);
        } catch (\Exception $err) {
            DB::rollback();
            return response()->json(['status' => 'error', 'message' => $err->getMessage()], 400);
        }
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);

        $image_name = $user->image;
        $image_name ? File::delete(storage_path('app/public/users/' . $image_name)) : '';

        $user->delete();

        return response()->json(['status' => 'success', 'data' => $user, 'message' => 'Berhasil menghapus user'], 200);
    }

    public function getUserLogin()
    {
        $user = Auth::user();
        return response()->json(['status' => 'success', 'data' => $user, 'message' => 'Berhasil menampilkan user login'], 200);
    }
}
