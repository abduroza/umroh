<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{Transaction, TransactionDetail, UmrohPackage, User};
use App\Http\Resources\OrderCollection; //untuk men-format data dari hasil query yang telah dilakukan, dimana data yang diambil akan kita batas hanya 10 data dalam sekali query menggunakan fungsi paginate()
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use DB;
use File;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Transaction::with(['transaction_details.user', 'umroh_package', 'user', 'payments.bank_account'])->orderBy('created_at', 'DESC');
        $search = request()->q;
        $status = request()->status;
        $user = Auth::user();

        if($search != ''){
            $orders = $orders->whereHas('umroh_package', function($query) use ($search) {
                    $query->where('title', 'LIKE', '%'.$search.'%');
                });
        }

        $orders = $orders->where('user_id', $user->id);

        if(in_array($status, [0,1,2,3,4,5])){
            //maka ambil data berdasarkan status tersebut
            $orders = $orders->where('status', $status);
        }

        $orders = $orders->paginate(10);

        return new OrderCollection($orders);
    }
}
