<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\UmrohPackage;
use App\Models\BankAccount;
use App\Models\TransactionDetail;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use DB;
use Mail;
use App\Mail\CheckoutSuccess;

class CheckoutController extends Controller
{
    public function index($transaction_id)
    {
        $transaction = Transaction::with(['user', 'umroh_package', 'transaction_details.user'])
            ->findOrFail($transaction_id);

        return response()->json(['status' => 'success', 'data' => $transaction, 'message' => 'Menampilkan transaksi'], 200);
    }

    public function addToCart($umroh_package_id)
    {
        DB::beginTransaction();
        try {
            $user = Auth::user();
            $umroh_package = UmrohPackage::findOrFail($umroh_package_id);
            
            $transaction = Transaction::create([
                'user_id' => $user->id,
                'umroh_package_id' => $umroh_package_id,
                'additional_visa' => 100, //karena is_visa pada tr_detail di set 1 (true) shg dikasih harga $100
                'total' => $umroh_package->price + 100,
                'status' => 0, // 0: in_cart, 1: pending, 2: success, 3: failed, 4: cancel, 5:completed
            ]);
            
            $transaction_details = TransactionDetail::create([
                'transaction_id' => $transaction->id,
                'user_id' => $user->id,
                'is_visa' => 1,
                'doe_passport' => Carbon::now()->addYears(5) //ini jg masih keliru
            ]);

            $transaction->load('transaction_details'); //supaya response yg dikirim juga menyertakan table transaction_details. relationship transaction_details harus dibuat dulu di model Transaction.php
            
            DB::commit();
            return response()->json(['status' => 'success', 'data' => $transaction, 'message' => 'Berhasil menambah ke cart'], 201);
        } catch (\Exception $err) {
            DB::rollback();
            return response()->json(['status' => 'errors', 'message' => $err->getMessage()], 400);
        }
    }

    public function addMember(Request $request, $transaction_id)
    {
        $this->validate($request, [
            'email' => 'required|exists:users,email',
            'is_visa' => 'required|boolean',
            'doe_passport' => 'required'
        ], [
            'required' => 'Wajib diisi',
            'exists' => 'Tidak ditemukan / terdaftar'
        ]);       

        DB::beginTransaction();
        try {
            $transaction = Transaction::with(['umroh_package'])->findOrFail($transaction_id);

            $user = User::where('email', $request->email)->first();
            $transaction_detail = TransactionDetail::create([
                'transaction_id' => $transaction_id,
                'user_id' => $user->id,
                'is_visa' => $request->is_visa,
                'doe_passport' => $request->doe_passport
            ]);

            if($request->is_visa){
                $transaction->additional_visa += 100;
                $transaction->total += 100;
            }

            $transaction->total += $transaction->umroh_package->price;

            $transaction->save();
            
            DB::commit();
            return response()->json(['status' => 'success', 'data' => $transaction, 'message' => 'Berhasil menambah member'], 201);
        } catch (\Exception $err) {
            DB::rollback();
            return response()->json(['status' => 'errors', 'message' => $err->getMessage()], 400);
        }
    }

    public function removeMember(Request $request, $transaction_detail_id)
    {
        $transaction_detail = TransactionDetail::findOrFail($transaction_detail_id);

        $transaction = Transaction::with(['transaction_details', 'umroh_package'])->findOrFail($transaction_detail->transaction_id);

        // jika is_visa bernilai true
        if($transaction_detail->is_visa){
            $transaction->total -= 100;
            $transaction->additional_visa -= 100;
        }

        $transaction->total -= $transaction->umroh_package->price;

        $transaction->save();
        $transaction_detail->delete();

        return response()->json(['status' => 'success', 'data' => $transaction, 'message' => 'Berhasil menghapus member'], 200);
    }

    public function cancelBooking(Request $request, $transaction_id)
    {
        DB::beginTransaction();
        try {
            // $transaction_detail = TransactionDetail::where('transaction_id', $transaction_id)->delete();
            $transaction = Transaction::with(['umroh_package'])->findOrFail($transaction_id);

            $umroh_package = $transaction->umroh_package;
            $transaction->update([
                'status' => 4 //cancel
            ]);
            // $transaction->delete();

            DB::commit();
            return response()->json(['status' => 'success', 'data' => $umroh_package, 'message' => 'Berhasil membatalkan transaksi'], 200);
        } catch (\Throwable $err) {
            DB::rollback();
            return response()->json(['status' => 'errors', 'message' => $err->getMessage()], 400);
        }
    }

    public function success(Request $request, $transaction_id)
    {
        $transaction = Transaction::with(['user', 'umroh_package.galleries', 'transaction_details.user'])->findOrFail($transaction_id);
        $transaction->status = 1; //pending pembayaran
        $transaction->save();

        $banks = BankAccount::all();

        // kirim email ke user
        Mail::to($transaction->user->email)->send(
            new CheckoutSuccess($transaction, $banks)
        );

        return response()->json(['status' => 'success', 'data' => $transaction, 'message' => 'Berhasil checkout'], 200);
    }

}
