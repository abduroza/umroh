<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UmrohPackage;

class DetailController extends Controller
{
    public function index($slug)
    {
        $umroh_package = UmrohPackage::with(['galleries'])
            ->where('slug', $slug)
            ->firstOrFail();
        return response()->json(['status' => 'success', 'data' => $umroh_package]);
    }
}
