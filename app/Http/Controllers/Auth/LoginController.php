<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\User;

class LoginController extends Controller 
{
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|exists:users,email', //jika input user error, maka akan menampilkan message sesuai kesalahan, termasuk email sudah terdaftar atau belum. tp di postman tidak terdetek
            'password' => 'required'
        ]);

        //ambil semua request (email, password) kecuali remmeber_me
        $auth = $request->except(['remember_me']);

        //melakukan otentikasi
        if(auth()->attempt($auth, $request->remember_me)){
            //apabila berhasil generate api_token menggunakan str random
            auth()->user()->update(['api_token' => Str::random(40)]);

            //kirim response ke client utk diproses lebih lanjut
            return response()->json(['status' => 'success', 'data' => auth()->user(), 'message' => 'Login berhasil'], 200);
        }

        return response()->json(['status' => 'failed', 'message' => 'Email atau password salah']);
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|string|unique:users,email',
            'password' => 'required|string',
            'phone' => 'required|string|min:9',
            'birth_date' => 'required|string',
            'gender' => 'required|boolean',
            'address' => 'required|string',
            'photo' => 'nullable|image',
        ],[
            'required' => 'wajib diisi',
            'unique' => 'sudah terdaftar',
            'image' => 'harus berformat jpg, png atau jpeg'
        ]);

        $image_name = null;

        if($request->hasFile('photo')){
            $file = $request->file('photo');
            $image_name = Str::slug($request->name) . '-' . time() . '.' . $file->getClientOriginalExtension();
            $file->storeAs('public/users', $image_name); //file disimpam di storage/app/public/users
        }

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'roles' => 2,
            'phone' => $request->phone,
            'birth_date' => $request->birth_date,
            'gender' => $request->gender,
            'address' => $request->address,
            'photo' => $image_name,
        ]);

        $auth = $request->only('email', 'password');

        //melakukan otentikasi
        if(auth()->attempt($auth, $request->remember_me)){
            //apabila berhasil generate api_token menggunakan str random
            auth()->user()->update(['api_token' => Str::random(40)]);

            //kirim response ke client utk diproses lebih lanjut
            return response()->json(['status' => 'success', 'data' => auth()->user(), 'message' => 'Pendaftaran berhasil'], 201);
        }

        return response()->json(['status' => 'success', 'message' => 'Pendaftaran berhasil']);
    }
}
