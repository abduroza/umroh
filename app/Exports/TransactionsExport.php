<?php

namespace App\Exports;

use App\Models\Transaction;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class TransactionsExport implements FromView, ShouldAutosize
{
    protected $transactions;
    protected $month;
    protected $year;

    //request data transactions, bulan dan tahun dari DashboardController.php
    public function __construct($transactions, $month, $year)
    {
        $this->transactions = $transactions;
        $this->month = $month;
        $this->year = $year;
    }
    
    public function view(): View
    {
        //meload view transaction.blade.php dengan mengirimkan data yg direquest diatas
        return view('export.transaction', [
            'transactions' => $this->transactions,
            'month' => $this->month,
            'year' => $this->year
        ]);
    }
}
