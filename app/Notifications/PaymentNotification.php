<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\BroadcastMessage;

class PaymentNotification extends Notification implements ShouldQueue
{
    use Queueable;
    // mendefinisikan variabel secara global
    protected $payment;
    protected $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($payment, $user) //parameter ini menangkap value dari PaymentController.php 
    {
        $this->payment = $payment; //mengambil data payment, kemudian men-assign ke variabel diatas untuk 
        $this->user = $user; //mengambil data user yg sedang login/mengirimkan notifikasi
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        //menggunakan dua metode yaitu menyimpan ke database dan broadcast ke pusher
        //karena antara yg disimpan ke database dan ke broadcast pusher berbeda form
        //jika form sama, maka bisa pakai satu method saja yaitu toArray()
        return ['database', 'broadcast'];
    }

    //form data yg disimpan ke database
    public function toDatabase($notifiable)
    {
        return [
            //mendefinisikan apa saja yg akan dikirim ke table notifications->data
            'sender_id' => $this->user->id,
            'sender_name' => $this->user->name,
            'sender_photo' => $this->user->photo,
            'payment' => $this->payment
        ];
    }

    //form data yg disimpan ke broadcast pusher
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'sender_id' => $this->user->id,
            'sender_name' => $this->user->name,
            'sender_photo' => $this->user->photo,
            'payment' => $this->payment
        ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
