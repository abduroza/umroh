<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;

class UmrohPackage extends Model
{
    use HasFactory, UsesUuid;

    protected $guarded = [];

    public function galleries(){
        return $this->hasMany(Gallery::class, 'umroh_package_id');
    }
}
