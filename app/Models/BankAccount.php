<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;

class BankAccount extends Model
{
    use HasFactory, UsesUuid;

    protected $guarded = [];

    public function payments(){
        return $this->hasMany(Payment::class, 'payment_id');
    }
}
