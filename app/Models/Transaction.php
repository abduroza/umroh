<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;

class Transaction extends Model
{
    use HasFactory, UsesUuid;

    protected $guarded = [];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function umroh_package(){
        return $this->belongsTo(UmrohPackage::class, 'umroh_package_id');
    }

    public function transaction_details(){
        return $this->hasMany(TransactionDetail::class, 'transaction_id');
    }

    public function payments(){
        return $this->hasMany(Payment::class, 'transaction_id');
    }
}
