<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;

class Gallery extends Model
{
    use HasFactory, UsesUuid;

    protected $guarded = [];

    public function umroh_package(){
        return $this->belongsTo(UmrohPackage::class, 'umroh_package_id');
    }
}
