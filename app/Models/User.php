<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\UsesUuid;

class User extends Authenticatable
{
    use HasFactory, Notifiable, UsesUuid;

    protected $guarded = [];

    protected $hidden = [
        'password',
        'remember_token',
        // 'api_token'
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function transactions(){
        return $this->hasMany(Transaction::class, 'user_id');
    }

    public function payments(){
        return $this->hasMany(Payment::class, 'user_id');
    }

    public function transaction_details(){
        return $this->hasMany(TransactionDetail::class, 'user_id');
    }
}
