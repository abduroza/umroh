<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CheckoutSuccess extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    public $banks;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($transaction, $banks)
    {
        $this->data = $transaction;
        $this->banks = $banks;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from('umrohtravel@gmail.com')
            ->subject('Petunjuk Pembayaran Umroh.id')
            ->view('email.checkout-success');
    }
}
